import tensorflow as tf
if tf.__version__ <= '2.17.0':
	print(f"Tensorflow imported successfully. Tensorflow version -> {tf.__version__}")
else:
	print("Tensorflow version mismatch!. Please check tensorflow isn't imported before installing quick_ml. Restart the session to fix the error.")

# from quick_ml import tfrecords_maker
# from quick_ml import begin_tpu
# from quick_ml import visualize_and_check_data
# from quick_ml import load_models_quick
# from quick_ml import training_predictions
# from quick_ml import predictions
# from quick_ml import augments
# from quick_ml import k_fold_training

#from quick_ml import Classification
#from quick_ml import Segmentation
#from quick_ml.Video import *

#__all__ = ['Segmentation', 'video']

from . import Segmentation
from . import video

#import quick_ml.data_preprocessing.remove_imbalance
__version__ = '1.3.20'
